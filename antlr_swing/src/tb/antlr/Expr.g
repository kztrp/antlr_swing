grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;


stat
    : expr NL -> expr
    | PRINT expr NL -> ^(PRINT expr)
    | VAR ID NL -> ^(VAR ID)
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | ID PODST expr NL -> ^(PODST ID expr)
    | NOT expr NL -> ^(NOT expr)
    | LB
    | RB
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | AND^ atom
      | OR^ atom
      | XOR^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    
    ;

VAR :'var';
//drukowanie 
PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	//definicja operacji logicznych AND, OR, XOR, NOT
AND
  : '&'
  ;
OR
  : '|'
  ;
XOR
  : '^'
  ;
NOT
  : '!'
  ;
LB

  : '{'

  ;


RB

  : '}'

  ;
