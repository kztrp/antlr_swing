tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (print | declGlobVar | setGlobVar)
          ;

print   :
// drukuje wynik tylko jeżli napotka PRINT (słowo kluczowe 'print')
          ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
// w innym przypadku drukuje jedynie tekst działania (ale nie wynik)
          | e=expr {drukuj ($e.text);}
        ;  
declGlobVar
  : ^(VAR i1=ID) {declGlobal($ID.text);}
;
 catch [RuntimeException ex] {myError(ex, $i1);}

setGlobVar
  :  ^(PODST i1=ID e2=expr) {setGlobal($i1.text, $e2.out);}
;
 catch [RuntimeException ex] {myError(ex, $i1);}


expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getGlobal($ID.text);}
        //operacje logiczne
        | ^(AND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(OR   e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | ^(XOR   e1=expr e2=expr) {$out = $e1.out ^ $e2.out;}
        | ^(NOT   e2=expr) {$out = ~$e2.out;}

        ;
