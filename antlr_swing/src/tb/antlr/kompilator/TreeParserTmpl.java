/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;


public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols mLocalSymbols = new LocalSymbols();

	protected int depth = 0;

	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	//zwiększa wartość głębii i wywołuje metodę enterScope
	protected void enterScope() {

		mLocalSymbols.enterScope();

		++depth;

	}
	//zmnięjsza wartość głębii i wywołuje metodę leaveScope
	protected void leaveScope() {

		mLocalSymbols.leaveScope();

		--depth;

	}
	// dopisuje do nazwy dwa podkreślenia oraz głębię, na której została zdefiniowana zmienna 
	protected String getLocalName(String name) {

		return name + "__" + depth;

	}

}
